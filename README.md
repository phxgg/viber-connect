# @universis/viber-connect

[Universis](https://gitlab.com/universis/universis-api) instant messaging plugin for Viber 

## Installation

npm i @universis/viber-connect

## Usage

Register `ViberConnectService` in application services

    # app.production.json

    "services": [
        ...,
        {
            "serviceType": "@universis/viber-connect#ViberConnectService"
        }
    ]

Add `ViberConnectSchemaLoader` to schema loaders

    # app.production.json
    
    {
        "settings": {
            "schema": {
                "loaders": [
                    ...,
                    {
                        "loaderType": "@universis/viber-connect#ViberConnectSchemaLoader"
                    }
                ]
            }
        }
    }
